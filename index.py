import os

from datetime import datetime
from ftplib import FTP

import settings

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from models import Docs

engine = create_engine(settings.DATABASE_DSN, echo=False)
session = sessionmaker(bind=engine)()

FTP = FTP(host='localhost')
FTP.login(user='ronald', passwd='123')

docs = []
supported_extensions = ('.doc', '.ppt', '.xls', '.html', '.htm')


def is_dir(FTP, name):
    try:
        FTP.cwd(name)
        FTP.cwd(len(name.split('/')) * '../')
        return True
    except:
        return False


def get_docs(root):
    for name in FTP.nlst(root):
        if is_dir(FTP, name):
            get_docs(name)
        else:
            name_ext = os.path.splitext(name)[1]
            if name_ext in supported_extensions:
                dtime = FTP.sendcmd('MDTM %s' % name)
                docs.append(dict(path=name,
                    modified_at=datetime.strptime(
                        dtime[4:], "%Y%m%d%H%M%S")))
    return docs


docs_ftp = get_docs('test')
FTP.quit()

docs_db = session.query(Docs).all()
tmp = []

for ftpdoc in docs_ftp:
    ftp_path = ftpdoc.get('path')
    ftp_modified_at = ftpdoc.get('modified_at')

    find = False
    for dbdoc in docs_db:
        db_id = dbdoc.id
        db_path = dbdoc.path
        db_modified_at = dbdoc.modified_at

        if ftp_path == db_path:
            find = True

            if db_id in tmp:
                tmp.remove(db_id)

            doc = session.query(Docs).get(db_id)
            del docs_db[docs_db.index(doc)]

            if ftp_modified_at > db_modified_at:
                doc = session.query(Docs).get(db_id)
                doc.modified_at = ftp_modified_at
                session.add(doc)
                session.commit()
            break
        else:
            if not db_id in tmp:
                tmp.append(db_id)

    if not find:
        doc = Docs()
        doc.path = ftp_path
        doc.modified_at = ftp_modified_at
        session.add(doc)
        session.commit()

for db_id in tmp:
    doc = session.query(Docs).get(db_id)
    session.delete(doc)
    session.commit()


"""
db = [78, 2, 1]
ftp = [1, 3, 5]
tmp = []
for x in ftp:
    find = False
    for y in db:
        if x == y:
            find = True
            if y in tmp:
                tmp.remove(y)
            db.remove(y)
            break
        else:
            if not y in tmp:
                tmp.append(y)

    if find == False:
        print 'save: ', x
print 'delete: ', tmp
"""
